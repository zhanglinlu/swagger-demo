package com.example.swaggerdemo.controller;

import com.example.swaggerdemo.model.dto.TestParamDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 测试接口
 * @Author ZhangLinlu
 * @Date 2020/10/10
 **/
@Api(tags = "测试接口")
@RestController
public class TestController {

    @ApiOperation(value = "我是test1接口",notes = "notes：我是test1")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "name",value = "姓名",required = true,paramType = "query",dataType = "String",defaultValue = "zll")
    })
    @GetMapping("test1")
    public Object test1(String name) {
        return "success" + name;
    }

    @ApiOperation(value = "我是test2接口",notes = "notes：我是test2")
    @GetMapping("test2")
    public Object test2(TestParamDTO testParamDTO) {
        return "success" + testParamDTO.getName();
    }


}
