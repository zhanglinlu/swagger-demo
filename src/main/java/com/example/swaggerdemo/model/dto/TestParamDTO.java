package com.example.swaggerdemo.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description 测试javabean参数
 * @Author ZhangLinlu
 * @Date 2020/10/10
 **/
@Data
@ApiModel(value = "测试javabean参数")
public class TestParamDTO {

    @ApiModelProperty(name = "name",value = "姓名",required = true)
    private String name;
}
